from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from .serializers import UserSerializer
from .models import User
from common.permissions import IsOwner, IsAdminOnly


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        if self.action in ["list", "HEAD", "OPTIONS"]:
            permission_classes = [IsAdminOnly]
        elif self.action in ["create", "HEAD", "OPTIONS"]:
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsOwner, IsAdminOnly]
        return [permission() for permission in permission_classes]
