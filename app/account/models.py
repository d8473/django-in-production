import uuid
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

# Custom Imports
from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    """Custom User models that use email instead of username for login"""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField(max_length=30, verbose_name="First Name")
    last_name = models.CharField(max_length=30, verbose_name="Last Name")
    email = models.EmailField(unique=True, max_length=60, verbose_name="Email")
    date_joined = models.DateTimeField(verbose_name="date joined", auto_now_add=True)
    last_login = models.DateTimeField(verbose_name="last login", auto_now=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = "email"
    objects = UserManager()

    @property
    def full_name(self):
        """Return user's full name"""
        return f"{self.first_name} {self.last_name}"

    def __str__(self):
        return self.email

    class Meta:
        ordering = ["id", "first_name", "last_name"]
