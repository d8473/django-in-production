from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User


@admin.register(User)
class UserAdmin(UserAdmin):
    list_display = ("id", "email", "is_active")
    ordering = ("-date_joined",)

    readonly_fields = ("date_joined", "last_login")
    list_filter = ()

    fieldsets = (
        (None, {"fields": ("email", "password")}),
        ("Basic Info", {"fields": ("first_name", "last_name")}),
        (
            "Permissions",
            {"fields": ("is_active", "is_staff", "is_superuser")},
        ),
    )

    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.

    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide", "extrapretty"),
                "fields": ("email", "first_name", "last_name"),
            },
        ),
        ("Security", {"fields": ("password1", "password2")}),
        (
            "Permissions",
            {"fields": ("is_active", "is_staff", "is_superuser")},
        ),
    )
