import os
from .common import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
SECRET_KEY = "django-insecure-@yx9(up39kv%l2)(mu394v%-=sh20-$p9jgnuh&phs5zxlqy-h"

# Install extra apps for developement & testing.
INSTALLED_APPS.extend(["debug_toolbar"])


# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}

CELERY_BROKER_URL = "redis://redis:6379/0"
CELERY_RESULT_BACKEND = "redis://redis:6379/0"

# Channels
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [(os.environ.get("CHANNELS_REDIS", "redis://127.0.0.1:6379/0"))],
        },
    },
}
