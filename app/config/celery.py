"""
https://docs.celeryq.dev/en/stable/django/first-steps-with-django.html
"""
import os
from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.dev")

app = Celery("config")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


@app.task
def divide(x, y):
    import time

    time.sleep(200)
    return x / y
