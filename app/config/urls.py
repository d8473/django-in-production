import os
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from config.settings import common

ENVIRONMENT = os.environ.get("DJANGO_SETTINGS_MODULE")

urlpatterns = [
    path("admin/", admin.site.urls),
    path("auth/", include("authentication.urls")),
    path("account/", include("account.urls")),
]

if ENVIRONMENT == "config.settings.dev":  # if true, means local environment
    urlpatterns.extend(
        [
            path("__debug__/", include("debug_toolbar.urls")),
            path("playground/", include("playground.urls")),
        ]
    )
    urlpatterns += static(common.MEDIA_URL, document_root=common.MEDIA_ROOT)
