from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from playground.tasks import sample_task


# Create your views here.
class SubscribeView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """
        Use Websocket to get notification of Celery task, instead of using ajax polling
        """
        task = sample_task.delay("azhariqbal2942@gmail.com")
        return Response(data={"task_id": task.task_id})
