import pytest
from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.authtoken.models import Token

from account.models import User


@pytest.mark.django_db
class TestTaskCreate:
    client = APIClient()

    def test_if_user_is_annonymous_returns_401(self):
        client = self.client
        response = client.get("/playground/task/")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_create_user_with_valid_credentials_return_201(self):
        client = APIClient()
        response = client.post(
            "/account/user/",
            {
                "first_name": "Azhar",
                "last_name": "Iqbal",
                "email": "azhariqbal2942@gmail.com",
                "is_staff": False,
                "password": "89565ffe",
            },
        )

        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.skip
    def test_if_user_is_authenticated_returns_200(self):
        client = self.client
        self.user = User.objects.create_user(
            "abc@gmail.com", "89565ffe", first_name="Azhar", last_name="Iqbal"
        )

        token, created = Token.objects.get_or_create(user=self.user)
        client.credentials(HTTP_AUTHORIZATION=f"Token {token}")
        client.get(
            "/playground/task/",
        )

    def test_create_user_with_invalid_email_return_400(self):
        client = self.client
        response = client.post(
            "/account/user/",
            {
                "first_name": "Azhar",
                "last_name": "Iqbal",
                "email": "azhariqbal2942",
                "is_staff": False,
                "password": "89565ffe",
            },
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST
