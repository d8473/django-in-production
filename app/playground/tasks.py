from celery import shared_task
from celery.signals import task_postrun
from playground.consumers import notify_channel_layer


@shared_task()
def sample_task(email):
    from playground.utils.helper import api_call

    api_call(email)


@shared_task(name="task_clear_session")
def task_clear_session():
    print("Session Cleared")
    # from django.core.management import call_command
    # call_command('clearsessions')


@task_postrun.connect
def task_postrun_handler(task_id, **kwargs):
    """
    When celery task finish, send notification to Django channel_layer, so Django channel would receive
    the event and then send it to web client
    """
    notify_channel_layer(task_id)
