from django.urls import path

from .views import SubscribeView

urlpatterns = [path("task/", SubscribeView.as_view(), name="ws")]  # Task starter
