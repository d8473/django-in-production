from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    """
    Custom permission to only give the owner of the object access
    """

    message = "You must be the owner of this object"

    def has_object_permission(self, request, view, obj):
        return obj == request.user or request.user.is_staff


class IsAdminOnly(permissions.BasePermission):
    """
    Allows access only to admin users.
    """

    message = "Access denied"

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_staff)
